#ifndef BMP_FORMAT_H
#define BMP_FORMAT_H

#include "image.h"
#include <inttypes.h>
#include <stdint.h>

#define BMP_SIGNATURE 19778
#define PADDING_BYTES 4
#define COUNT_OF_BYTES_IN_PIXEL 3

struct __attribute__((packed)) bmp_header 
{
    uint16_t bf_type;
    uint32_t bfile_size;
    uint32_t bf_reserved;
    uint32_t b_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};

void print_bmp_header (const struct bmp_header bmp_header);
uint8_t calculate_skipped_bytes(uint32_t width);
void update_header(struct bmp_header *header, struct image image);

#endif
