#ifndef FILE_WRITER_H
#define FILE_WRITER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool write_uint16(FILE* file, uint16_t variable);
bool write_uint8(FILE* file, uint8_t variable);
bool write_uint32(FILE* file, uint32_t variable);

#endif
