#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image
{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct pixel* allocate_memory_pixels(uint32_t width, uint32_t height);

struct image rotate_image_counterclockwise(struct image source);

struct image rotate_image_clockwise(struct image source);

struct image horizon_reflect_image(struct image source);

struct image flip_image(struct image source);

struct image vertical_reflect_image(struct image source);

void free_image(struct image* image);

#endif
