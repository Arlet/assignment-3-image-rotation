#ifndef BMP_WRITER_H
#define BMP_WRITER_H

#include "bmp_format.h"
#include "file_writer.h"
#include "image.h"
#include <stdbool.h>

enum write_status 
{
    WRITE_SUCCESS,
    HEADER_WRITE_ERROR,
    PIXEL_WRITE_ERROR,
    FILE_WRITE_ERROR
};

enum write_status write_bmp_file (FILE* file, const struct bmp_header header, const struct image image);

#endif
