#ifndef FILE_READER_H
#define FILE_READER_H

#include "optional.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

struct maybe_uint32 read_uint32(FILE* file);
struct maybe_uint16 read_uint16(FILE* file);
struct maybe_uint8 read_uint8(FILE* file);

bool read_some_uint8(FILE* file, size_t count, uint8_t* pvalues);

bool set_uint32(FILE* file, uint32_t* variable);
bool set_uint16(FILE* file, uint16_t* variable);
bool set_uint8(FILE* file, uint8_t* variable);

#endif
