#ifndef BMP_READER_H
#define BMP_READER_H

#include <inttypes.h>
#include <stdint.h>

#include "bmp_format.h"
#include "file_reader.h"
#include "image.h"

enum read_status
{
    READ_SUCCESS,
    BYTE_READ_ERROR,
    IT_IS_NOT_BMP,
    FILE_READ_ERROR,
    MEMORY_ALLOCATE_ERROR
};

enum read_status read_bmp_header(FILE* file, struct bmp_header* header);
enum read_status read_pixels(const struct bmp_header bmp_header, FILE* file, struct image* image);

#endif
