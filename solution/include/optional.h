#ifndef OPTIONAL_H
#define OPTIONAL_H

#include <stdint.h>

struct maybe_uint32
{
    uint8_t valid;
    uint32_t value;
};

struct maybe_uint16
{
    uint8_t valid;
    uint16_t value;
};

struct maybe_uint8
{
    uint8_t valid;
    uint8_t value;
};

extern const struct maybe_uint32 none_uint32;
extern const struct maybe_uint16 none_uint16;
extern const struct maybe_uint8 none_uint8;

struct maybe_uint32 some_uint32(uint32_t value);
struct maybe_uint16 some_uint16(uint16_t value);
struct maybe_uint8 some_uint8(uint8_t value);

#endif
