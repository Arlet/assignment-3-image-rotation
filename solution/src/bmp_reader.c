#include "bmp_reader.h"
#include <malloc.h>
#include <stdio.h>


enum read_status read_bmp_header(FILE* file, struct bmp_header* header) // 54 байта - xdd
{
    if(fread(header, sizeof(struct bmp_header), 1, file) == 0)
        return BYTE_READ_ERROR;

    if(header->bf_type != BMP_SIGNATURE)
        return IT_IS_NOT_BMP;

    return READ_SUCCESS;
}

enum read_status read_pixels(const struct bmp_header header, FILE* file, struct image* image)
{
    if (fseek(file, header.b_off_bits, SEEK_SET) != 0)
        return FILE_READ_ERROR;

    const uint8_t count_of_skipped_bytes = calculate_skipped_bytes(header.bi_width);

    struct pixel* pixels = malloc(sizeof(struct pixel)*header.bi_height*header.bi_width);

    if (pixels == NULL)
        return MEMORY_ALLOCATE_ERROR;

    uint8_t tb, tg, tr;

    for (uint64_t i = 0; i<header.bi_height; i++)
    {
        for (uint64_t j=0;j<header.bi_width; j++)
        {
            if (!set_uint8(file, &tb) || !set_uint8(file, &tg) || !set_uint8(file, &tr))
            {
                free(pixels);
                return BYTE_READ_ERROR;
            }
            
            pixels[i*header.bi_width+j] = (struct pixel) {.b = tb, .g = tg, .r = tr}; 
        }
        for (uint8_t k=0; k<count_of_skipped_bytes; k++)
            read_uint8(file);
    }

    *image = (struct image) {.width = header.bi_width, .height = header.bi_height, .data = pixels};

    return READ_SUCCESS;
}
