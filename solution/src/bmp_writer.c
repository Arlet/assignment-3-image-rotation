#include "bmp_writer.h"

static bool write_header(FILE* file, const struct bmp_header header);

enum write_status write_bmp_file (FILE* file, const struct bmp_header header, const struct image image)
{
    if(!write_header(file, header))
        return HEADER_WRITE_ERROR;

    const uint8_t count_of_skipped_bytes = calculate_skipped_bytes(header.bi_width);

    uint64_t byte_number;

    if (fseek(file, header.b_off_bits, SEEK_SET) != 0)
        return FILE_WRITE_ERROR;

    for (uint64_t i=0;i<image.height;i++)
    {
        for (uint64_t j=0;j<image.width;j++)
        {
            byte_number = i*image.width+j;
            if (!write_uint8(file, image.data[byte_number].b) || 
                !write_uint8(file, image.data[byte_number].g) || 
                !write_uint8(file, image.data[byte_number].r)
                )
                return PIXEL_WRITE_ERROR;
            
        }

        for (uint8_t k=0; k<count_of_skipped_bytes;k++)
            write_uint8(file, 150*k); // мусор
    }

    return WRITE_SUCCESS;
}

static bool write_header(FILE* file, const struct bmp_header header)
{
    return fwrite(&header, sizeof(struct bmp_header), 1, file) == 1;
}
