#include "file_reader.h"

struct maybe_uint32 read_uint32(FILE* file)
{
    uint32_t value;
    if (fread(&value, sizeof(uint32_t), 1, file) == 0)
        return none_uint32;
    return some_uint32(value);
}

struct maybe_uint16 read_uint16(FILE* file)
{
    uint16_t value;
    if (fread(&value, sizeof(uint16_t), 1, file) == 0)
        return none_uint16;
    return some_uint16(value);
}

struct maybe_uint8 read_uint8(FILE* file)
{
    uint8_t value;
    if (fread(&value, sizeof(uint8_t), 1, file) == 0)
        return none_uint8;
    return some_uint8(value);
}

bool read_some_uint8(FILE* file, size_t count, uint8_t* pvalues)
{
    return fread(pvalues, sizeof(uint8_t), count, file) == count;
}

bool set_uint32(FILE* file, uint32_t* variable) 
{
    struct maybe_uint32 temp_uint32 = read_uint32(file);
    if (!temp_uint32.valid)
        return false;
    *variable = temp_uint32.value;
    return true;
}

bool set_uint16(FILE* file, uint16_t* variable) 
{
    struct maybe_uint16 temp_uint16 = read_uint16(file);
    if (!temp_uint16.valid)
        return false;
    *variable = temp_uint16.value;
    return true;
}

bool set_uint8(FILE* file, uint8_t* variable)
{
    struct maybe_uint8 temp_uint8 = read_uint8(file);
    if (!temp_uint8.valid)
        return false;
    *variable = temp_uint8.value;
    return true;
}
