#include "image.h"
#include <stdlib.h>

void free_image(struct image* image)
{
    if (image == NULL || image->data == NULL)
        return;
    
    free(image->data);
}

struct pixel *allocate_memory_pixels(uint32_t width, uint32_t height)
{
    return malloc(sizeof(struct pixel) * width * height);
}

struct image rotate_image_counterclockwise(struct image source)
{
    struct pixel* data = allocate_memory_pixels(source.width, source.height);

    if (data == NULL)
        return source;

    const uint64_t width = source.height;
    const uint64_t height = source.width;

    for(uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j=0; j<width; j++)
        {
            data[i*width+j] = source.data[(source.height-1-j)*source.width+i];
        }
    }

    return (struct image) {.width = source.height, .height = source.width, .data = data};
}

struct image rotate_image_clockwise(struct image source)
{
    struct pixel *data = allocate_memory_pixels(source.width, source.height);

    if (data == NULL)
        return source;

    const uint64_t width = source.height;
    const uint64_t height = source.width;

    for(uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j=0; j<width; j++)
        {
            data[i*width+j] = source.data[j*source.width-i+source.width-1];
        }
    }

    return (struct image) {.width = source.height, .height = source.width, .data = data};    
}

struct image flip_image(struct image source)
{
    struct pixel *data = allocate_memory_pixels(source.width, source.height);

    if (data == NULL)
        return source;

    const uint64_t width = source.width;
    const uint64_t height = source.height; 

    for(uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j=0; j<width; j++)
        {
            data[i*width+j] = source.data[source.width*(source.height-1-i)+source.width-1-j];
        }
    }

    return (struct image) {.width = source.width, .height = source.height, .data = data}; 
}

struct image horizon_reflect_image(struct image source)
{
    struct pixel *data = allocate_memory_pixels(source.width, source.height);

    if (data == NULL)
        return source;

    const uint64_t width = source.width;
    const uint64_t height = source.height; 

    for(uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j=0; j<width; j++)
        {
            data[i*width+j] = source.data[source.width*i+source.width-1-j];
        }
    }

    return (struct image) {.width = source.width, .height = source.height, .data = data}; 
}

struct image vertical_reflect_image(struct image source)
{
    struct pixel *data = allocate_memory_pixels(source.width, source.height);

    if (data == NULL)
        return source;

    const uint64_t width = source.width;
    const uint64_t height = source.height; 

    for(uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j=0; j<width; j++)
        {
            data[i*width+j] = source.data[source.width*(source.height-1-i)+j];
        }
    }

    return (struct image) {.width = source.width, .height = source.height, .data = data}; 
}
