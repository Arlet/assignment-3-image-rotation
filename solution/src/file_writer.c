#include "file_writer.h"

bool write_uint16(FILE* file, uint16_t variable)
{
    return fwrite(&variable, sizeof(uint16_t), 1, file) == 1;
}

bool write_uint8(FILE* file, uint8_t variable)
{
    return fwrite(&variable, sizeof(uint8_t), 1, file) == 1;
}

bool write_uint32(FILE* file, uint32_t variable)
{
    return fwrite(&variable, sizeof(uint32_t), 1, file) == 1;
}
