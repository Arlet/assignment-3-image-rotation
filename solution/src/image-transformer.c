#include "bmp_reader.h"
#include "bmp_writer.h"
#include "image.h"
#include <stdio.h>

#define NOT_ENOUGH_ARGUMENTS_ERROR (-1)
#define FILE_NOT_FOUND_ERROR 1
#define SUCCESS_EXIT 0
#define HEADER_IS_CORRUPTED_ERROR 2
#define IT_IS_NOT_BMP_ERROR 666
#define READ_PIXEL_ERROR 3
#define MEMORY_ALLOCATION_ERROR 4
#define FILE_READING_ERROR 5

int main( int argc, char** argv ) 
{

    if (argc != 3)
    {
        printf("Not enough arguments. Please, enter input filename and output filename\n");
        return NOT_ENOUGH_ARGUMENTS_ERROR;
    }

    FILE* file = fopen(argv[1], "r");

    if (file == NULL) {
        printf("File not found\n");
        return FILE_NOT_FOUND_ERROR;
    }

    struct bmp_header header;

    switch(read_bmp_header(file, &header)) 
    {
        case BYTE_READ_ERROR:
        {
            printf("Header is corrupted\n");
            return HEADER_IS_CORRUPTED_ERROR;
        }
        case IT_IS_NOT_BMP:
        {
            printf("It's not a bmp file\n");
            return IT_IS_NOT_BMP_ERROR;
        }
        case MEMORY_ALLOCATE_ERROR:
        {
            printf("Not enough space on heap\n");
            return MEMORY_ALLOCATION_ERROR;
        }
        case FILE_READ_ERROR:
        {
            printf("Error with file reading\n");
            return FILE_READING_ERROR;
        }
        default:
            break;
    }

    print_bmp_header(header);

    struct image image;

    if (read_pixels(header, file, &image) != READ_SUCCESS) 
    {
        printf("Error with read pixels\n");
        return READ_PIXEL_ERROR;
    }

    if (fclose(file) != 0)
        printf("Error with file closing\n");

    printf("\n");

    struct image nimage = image;

    nimage = rotate_image_counterclockwise(nimage);
    //nimage = horizon_reflect_image(nimage);
    //nimage = rotate_image_clockwise(nimage);
    //nimage = flip_image(nimage);
    //nimage = vertical_reflect_image(nimage);

    update_header(&header, nimage);

    file = fopen(argv[2], "w");

    switch (write_bmp_file(file, header, nimage))
    {
        case WRITE_SUCCESS:
        {
            printf("File was created\n");
            break;
        }
        case HEADER_WRITE_ERROR:
        {
            printf("Header cannot be written\n");
            break;
        }
        case PIXEL_WRITE_ERROR:
        {
            printf("Pixels cannot be written\n");
            break;
        }
        case FILE_WRITE_ERROR:
        {
            printf("Error with file writing");
            break;
        }
        default:
        {
            printf("Something got wrong\n");
            break;
        }
    }

    if (fclose(file) != 0)
        printf("Error with file closing\n");

    free_image(&image);
    free_image(&nimage);

    return SUCCESS_EXIT;
}
