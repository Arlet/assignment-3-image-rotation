#include "bmp_format.h"
#include <stdio.h>
#include <stdlib.h>

void print_bmp_header (const struct bmp_header bmp_header)
{
    printf("bfType: %" PRIu16 "\n", bmp_header.bf_type);
    printf("bfileSize: %" PRIu32 "\n", bmp_header.bfile_size);
    printf("bfReserved: %" PRIu32 "\n", bmp_header.bf_reserved);
    printf("bOffBits: %" PRIu32 "\n", bmp_header.b_off_bits);
    printf("biSize: %" PRIu32 "\n", bmp_header.bi_size);
    printf("biWidth: %" PRIu32 "\n", bmp_header.bi_width);
    printf("biHeight: %" PRIu32 "\n", bmp_header.bi_height);
    printf("biPlanes: %" PRIu16 "\n", bmp_header.bi_planes);
    printf("biBitCount: %" PRIu16 "\n", bmp_header.bi_bit_count);
    printf("biCompression: %" PRIu32 "\n", bmp_header.bi_compression);
    printf("biSizeImage: %" PRIu32 "\n", bmp_header.bi_size_image);
    printf("biXPelsPerMeter: %" PRIu32 "\n", bmp_header.bi_x_pels_per_meter);
    printf("biYPelsPerMeter: %" PRIu32 "\n", bmp_header.bi_y_pels_per_meter);
    printf("biClrUsed: %" PRIu32 "\n", bmp_header.bi_clr_used);
    printf("biClrImportant: %" PRIu32 "\n", bmp_header.bi_clr_important);
}

uint8_t calculate_skipped_bytes(uint32_t width)
{
    return (PADDING_BYTES - (width * COUNT_OF_BYTES_IN_PIXEL) % PADDING_BYTES) % PADDING_BYTES;
}

void update_header(struct bmp_header *header, struct image image)
{
    header->bi_width = image.width;
    header->bi_height = image.height;
}
