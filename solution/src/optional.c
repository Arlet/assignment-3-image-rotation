#include "optional.h"

const struct maybe_uint32 none_uint32 = {0};
const struct maybe_uint16 none_uint16 = {0};
const struct maybe_uint8 none_uint8 = {0};

struct maybe_uint32 some_uint32(uint32_t value) 
{
    return (struct maybe_uint32) {.valid = 1, .value = value};
}

struct maybe_uint16 some_uint16(uint16_t value)
{
    return (struct maybe_uint16) {.valid = 1, .value = value};
}

struct maybe_uint8 some_uint8(uint8_t value)
{
    return (struct maybe_uint8) {.valid = 1, .value = value};
}
